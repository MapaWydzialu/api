const gulp = require('gulp');
const del = require('del');
const ts = require('gulp-typescript');
const tsconfig = require('./tsconfig.json');

var project = ts.createProject(tsconfig.compilerOptions);

function _compile() {
    return gulp.src(['./src/**/*.ts'])
        .pipe(project())
        .js.pipe(gulp.dest('dest'));
}

function _clean() {
    return del(['dest/**/*']);
}

function _watch() {
    let watcher = gulp.watch('src/**/*.ts', gulp.series(_compile));
}

const build = gulp.series(_clean, _compile);

gulp.task('compile', _compile);

gulp.task('clean', _clean);

gulp.task('watch', _watch);

gulp.task('default', build);