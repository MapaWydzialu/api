import {Request, Response} from 'express';
import { Database } from './database';
import { putMarkOnMap } from './PutMarkOnMap';

export const RoomByPersonController = async (request: Request, response: Response) => {
  if(request.query.name == undefined) {
    return response.status(400).json({
      error_code: 400,
      error_title: 'Bad request',
      error_body: 'Podaj imię, name=podane_imie'
    });
  }
  if(request.query.surname == undefined) {
    return response.status(400).json({
      error_code: 400,
      error_title: 'Bad request',
      error_body: 'Podaj nazwisko, surname=podane_nazwisko'
    });
  }
  let res = await (new Database()).query(`CALL FindRoomByPersonData('${request.query.name}', '${request.query.surname}');`);
  if(res.length == 0) {
    return response.status(404).json({
      error_code: 404,
      error_title: "Not Found Room",
      error_body: 'Nie znaleziono szukanego pokoju'
    });
  }
  res = res[0][0]
  res = await putMarkOnMap(res['X_Point'], res['Y_Point'], res['Z_Point']);
  return response.send(res);
}