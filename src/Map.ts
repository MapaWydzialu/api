import {Request, Response} from 'express';
import * as path from 'path';

export const MapController = async (request: Request, response: Response) => {

  return response
    .status(400)
    .json({'Error':'Select floor'});
}
export const MapFloorController = (request: Request, response: Response) =>  {
  switch(request.params.floor) {
    case '-1':
      return response.sendFile(path.join(__dirname, '../resources/', 'piwnica.svg'));
    case '0':
      return response.sendFile(path.join(__dirname, '../resources/', 'parter.svg'));
    case '1':
      return response.sendFile(path.join(__dirname, '../resources/', 'pietro-1.svg'));
    case '2':
      return response.sendFile(path.join(__dirname, '../resources/', 'pietro-2.svg'));
    case '3':
    return response.sendFile(path.join(__dirname, '../resources/', 'pietro-3.svg'));
    default:
      return response.redirect('/map');
  }
}