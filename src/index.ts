import * as express from 'express';
import { MapController, MapFloorController } from './Map';
import { RoomByPersonController } from './RoomsByPerson';
import { RoomByNumberController } from './RoomByNumber';
import { RoomByNameController } from './RoomByName';

const app = express();
global['port'] = process.env.PORT || 3000;
global['apiUrl'] = process.env.IP || 'localhost';

app.get('/map', MapController);
app.get('/map/:floor', MapFloorController);
app.get('/find/person', RoomByPersonController);
app.get('/find/room/number', RoomByNumberController);
app.get('/find/room/name', RoomByNameController);

app.listen(global['port'], ()=>{
  console.log(`Server run on port ${global['port']}`);
});
