import { Request, Response } from 'express';
import { Database } from './database';
import { putMarkOnMap } from './PutMarkOnMap';

export const RoomByNameController = async (request: Request, response: Response) => {
  if(request.query.name == undefined) {
    return response.status(400)
      .json({
        error_code: 400,
        error_title: 'Bad request',
        error_body: 'Podaj nazwe'
      });
  }
  console.log(request.query.name);
  let res = await(new Database()).query(`CALL FindRoomByName('${request.query.name}');`);
  if(res.length == 0) {
    return response.status(404).json({
      error_code: 404,
      error_title: "Not Found Room",
      error_body: 'Nie znaleziono szukanego pokoju'
    });
  }
  res = res[0][0];
  res = await putMarkOnMap(res['X_Point'], res['Y_Point'], res['Z_Point']);
  return response.send(res);
}