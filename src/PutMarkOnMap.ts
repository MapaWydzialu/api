import { Database } from './database';
import * as fetch from 'node-fetch';
import * as xml2js from 'xml2js';

export async function putMarkOnMap(x:number, y:number, z:number) {
  let builder = new xml2js.Builder();
  let parseString = xml2js.parseString;
  const http = await fetch(`http://${global['apiUrl']}:${global['port']}/map/${z}`);
  let res = await http.text();
  parseString(res, (err, result)=>{
    if(!result.svg.ellipse) {
      result.svg.ellipse = new Array();
    }
    result.svg.ellipse.push({
      $:{
        "style": "fill:#000000",
        "id": `ellipse${result.svg.ellipse.length}`,
        "cx": x,
        "cy": y,
        "rx": "2",
        "ry": "2"
      }
    });
    res = builder.buildObject(result);
  });
  return res;
}