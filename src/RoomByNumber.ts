import { Request, Response } from 'express';
import { Database } from './database';
import { putMarkOnMap } from './PutMarkOnMap';

export const RoomByNumberController = async (request: Request, response: Response) => {
  if(request.query.roomnumber == undefined) {
    return response.status(400)
      .json({
        error_code: 400,
        error_title: 'Bad request',
        error_body: 'Podaj numer pokoju'
      });
  }
  let res = await(new Database()).query(`CALL FindRoomByNumber(${request.query.roomnumber});`);
  if(res.length == 0) {
    return response.status(404).json({
      error_code: 404,
      error_title: "Not Found Room",
      error_body: 'Nie znaleziono szukanego pokoju'
    });
  }
  res = res[0][0];
  console.log(res);
  res = await putMarkOnMap(res['X_Point'], res['Y_Point'], res['Z_Point']);
  return response.send(res);
}