import * as MariaSQL from 'mariasql';

export class Database {
  constructor(
    private host = 'localhost',
    private port:number = 33061,
    private user = 'root',
    private password = '',
    private db = 'FacultyMap',
    private charset = 'utf8'
  ) {
    if(process.env.db) {
      this.db = process.env.db;
    }
    if(process.env.host) {
      this.host = process.env.host
    }
    if(process.env.port) {
      this.port = Number(process.env.port);
    }
    if(process.env.user) {
      this.user = process.env.user
    }
    if(process.env.password) {
      this.password = process.env.password
    }
  }

  query(statement:string) {
    return new Promise<any>(resolve=>{
      let con = new MariaSQL({
        host: this.host,
        port: this.port,
        user: this.user,
        password: this.password,
        db: this.db,
        charset: this.charset
      });
      // console.log(statement);
      con.query(statement, (err, rows)=>{
        if(err)
          throw err;
        // console.log(rows);
        resolve(rows);
      });
    });
  }

}